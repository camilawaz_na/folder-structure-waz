var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    fileinclude = require('gulp-file-include'),
    sequence = require('gulp-sequence'),
    jshint = require('gulp-jshint'),
    jshintStylish = require('jshint-stylish'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin');


// Development Tasks
gulp.task('includes', ['clean:html'], function() {
  return gulp.src('dev/files/pages/*.html')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: 'dev/files/includes'
    }))
    .pipe(gulp.dest('dev'))
});

var gulpPaths = {
  sass:'dev/files/scss/',
  cssDist:'dev/files/css/'
}

gulp.task('sass', function () {
  return gulp.src(gulpPaths.sass + '**/*.scss')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(['last 2 versions', 'ie 8', 'ie 9', '> 1%']))
    .pipe(gulp.dest(gulpPaths.cssDist))
    .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(gulpPaths.cssDist))
    .pipe(sourcemaps.write('.', {
      mapFile: function(mapFilePath) {
        return mapFilePath.replace('.css.map', '.map');
      }
    }))
    .pipe(gulp.dest(gulpPaths.cssDist))
    .pipe(browserSync.reload({stream:true}))
});

// Scripts
gulp.task('jsplugins', function() {
	return gulp.src('dev/files/js/plugins/*.js')
		.pipe(plumber({
			errorHandler: function(error) {
				console.log(error.message);
				this.emit('end');
			}
		}))
		.pipe(sourcemaps.init())
		.pipe(concat('plugins.js'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write('.', {
			mapFile: function(mapFilePath) {
				return mapFilePath.replace('.js.map', '.map');
			}
		}))
		.pipe(gulp.dest('dev/files/js'))
    .pipe(browserSync.reload({stream: true}))
});

// Scripts
gulp.task('scripts', function() {
	return gulp.src('dev/files/js/settings/*.js')
		.pipe(plumber({
			errorHandler: function(error) {
				console.log(error.message);
				this.emit('end');
			}
		}))
		.pipe(jshint({
			maxerr: 50,
			jquery: '$',
			esversion: 6
		}))
		.pipe(jshint.reporter('default'))
		.pipe(sourcemaps.init())
		.pipe(concat('settings.js'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write('.', {
			mapFile: function(mapFilePath) {
				return mapFilePath.replace('.js.map', '.map');
			}
		}))
		.pipe(gulp.dest('dev/files/js'))
    .pipe(browserSync.reload({stream: true}))
});

// Clean HTML
gulp.task('clean:html', function() {
  return gulp.src('dev/*.html')
    .pipe(clean());
});

// Start browserSync server
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'dev'
    }
  });
});

gulp.task('images', function() {
    gulp.src('dev/files/images/src/*')
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 4}),
      imagemin.svgo({
        plugins: [
            {removeViewBox: false},
            {cleanupIDs: false}
        ]
      })
    ]))
    .pipe(gulp.dest('dev/files/images'))
    .pipe(browserSync.reload({stream: true}))
});


gulp.task('watch', ['browserSync', 'includes', 'sass', 'scripts', 'jsplugins', 'images'], function(){
  gulp.watch('dev/files/scss/**/*.scss', ['sass']);
  gulp.watch('dev/files/**/*.html', ['includes']);
  gulp.watch('dev/files/js/settings/*.js', ['scripts']);
  gulp.watch('dev/files/js/plugins/*.js', ['jsplugins']);
  gulp.watch('dev/files/images/src/*', ['images']);
  gulp.watch('dev/*.html').on('change', browserSync.reload);
  gulp.watch('dev/files/font/*').on('change', browserSync.reload);
});

// Build Sequences
// ---------------
gulp.task('default', function(callback) {
  sequence(['includes', 'sass', 'scripts', 'jsplugins', 'images', 'watch'], callback);

});
