//SMOOTHSCROLL
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
      $('html,body').animate({
        scrollTop: target.offset().top -74
      }, 1000, 'easeInOutCubic');
      return false;
    }
  }
  });
});

// CONVERTING IMG SVG TO INLINE SVG
// Easy to manipulate with class
// To use just add class="svg" to your image
$('img.svg').each(function () {
 var $img = $(this);
 var imgID = $img.attr('id');
 var imgClass = $img.attr('class');
 var imgURL = $img.attr('src');
 $.get(imgURL, function (data) {
     // Get the SVG tag, ignore the rest
     var $svg = $(data).find('svg');
     // Add replaced image's ID to the new SVG
     if (typeof imgID !== 'undefined') {
         $svg = $svg.attr('id', imgID);
     }
     // Add replaced image's classes to the new SVG
     if (typeof imgClass !== 'undefined') {
         $svg = $svg.attr('class', imgClass + ' replaced-svg');
     }
     // Remove any invalid XML tags
     $svg = $svg.removeAttr('xmlns:a');
     // Replace image with new SVG
     $img.replaceWith($svg);
 }, 'xml');
});

// IMAGE LAZYLOAD
$('.lazy-img').each(function () {
 var $img = $(this);
 var imgURL = $img.attr('src');

 if(imgURL){
   $($img).attr('data-src', imgURL);
   $($img).removeAttr('src');
   $($img).attr('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');
 }

});
var bLazy = new Blazy({
    selector: 'img.lazy-img',
    offset: 300,
    successClass: 'lazy-loaded',
});


// OBJECT FIT CALL
$(function () {
  objectFitImages();
});

//FORMS ICONS

$('form').each(function () {
 var select = $(this).find('select:not([multiple=multiple])');
 var datepicker = $(this).find('.datepicker');

 // select.each(function () {
   select.parent().addClass('select-icon');
   datepicker.parent().addClass('datepicker-icon');
 // }

});


$(document).ready(function(){
  // things to do after the page load
});
