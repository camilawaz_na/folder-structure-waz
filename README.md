## Folder Structure
![Build Status](https://img.shields.io/badge/version-3.0-brightgreen.svg)
![Build Status](https://img.shields.io/badge/running-gulp-brightgreen.svg)

## How To

> First of all, make sure you have everything you need installed in your computer. (Node, Ruby, Gulp, etc)

1. Download folder
2. Using the command line navigate to that folder.
3. run ```npm install```
4. Relax and have a cup of coffee.
5. When thats done run ```gulp```

### Good To Know:

* Adding a include in the build: `@@include('include.html')`
* Find more tips inside the files


## General
* All folder names should be lowercase and use hyphens.
* CSS name convention in use: https://github.com/airbnb/css.
* Do not use ID selectors (they introduce an unnecessarily high level of specificity to your rule declarations, and they are not reusable).

## CSS Descriptions

###Base

The `scss/base/` folder holds all your base style for your project. In there, we have the reset (Normalize.css), custom typography, basic content style, and overall styles.
Be aware that in some of those files the CSS name convention is not used, because some of the selectors come from the CMS.

Example:

* `_reset.scss`
* `_typography.scss` custom fonts
* `_content.scss` lists, tables etc.
* `_forms.scss`

###Components

For components, there is the `scss/components/` folder . It can contain all kinds of specific modules like a map, a loader, a carousel, searchbox or anything along those lines.

Example:

* `_carousel.scss`
* `_map.scss`
* `_searchbox.scss`


### Plugins

The `scss/plugins/` folder contains all the CSS files from external libraries and frameworks – Slick Carousel, Fancybox and so on. These files should not be edited. Use your main stylesheets to overide any styles you need to change.

Example:

* `fancybox.scss`
* `slick.scss`



### Settings

The `scss/settings/` folder  gathers all Sass tools and helpers we’ll use across the project -  functions, mixins etc. This folder also contains a `_variables.scss` file  which holds all global variables for the project (for typography, color schemes, and so on).

Example:

* `_variables.scss`
* `_mixins.scss`
* `_main.scss`


###Theme
`scss/theme/` folder contains the general CSS code for the project. Also contains Pages/Layouts folders which contains page specific css e.g weddings, meetings etc.

Example:

* `_header.scss`
* `_footer.scss`


## JS Descriptions

###Plugins
`js/plugins/` contains all the JS files from external libraries and frameworks – bLazy, Slick Carousel, Fancybox and so on. These files should not be edited. Use your settings to overwrite any thing you need to change.
As the files are being minified with the gulp files, is a better practice put them unminified and each one in its own file.

Best Practices:

* Since the files are being minified with the `gulpfile`, put them unminified and each one in its own file.
* Specify the version being used in each file.

Example:

* `_slick.js`
* `_fancybox.js`

###Settings
`js/settings/` should contain all the JS files written by you.

Best Practices:

* Every component and/or page should have its own file.
* The page file should use more specific names. e.g: `$('.pagename .componentname').remove();`  
* You can create subfolders on this folder - but be careful.
* Its being compiled by alphabetic order - so be careful with that.

Example:

* `_main.js`
* `_header.js`

## IMG Descriptions

###Optimization (by Gulp)

To optimize the images we're using a gulp plugin `gulp-imagemin`

Best Practices:

* Original images are placed in `dev/files/images/scr`
* Optmize images are created by gulp in `dev/files/images`
* You can create change the levels of compression and other configurations on the gulfile - but be careful.
* gulp-imagemin documentation: https://www.npmjs.com/package/gulp-imagemin

###Inline SVG

In this structure you can find a function that inline svg's placed as images.

Best Practices:

* This function can be used when you need to manipulate the SVG properties (eg: fill color).
* All you need to do is put the class `svg` on your image, as bellow exemple.
* As this is a jQuery based function, use carefully and preferable bellow the fold, as it can change the style once the code is fully loaded.

Example:

* The input is: `<img src="images/placeholder/netaffinity-logo.svg" class="svg" alt="NetAffinity Logo"/>`
* The result will be:`<svg xmlns="http://www.w3.org/2000/svg" viewBox="-3844.78 -4607.9 112.78 49.29" class="center-block svg replaced-svg"><defs><style>.cls-1 { fill: #a2a2a2; }</style></defs><g id="logo-netaffinity"><path id="Path_3383" data-name="Path 3383" class="cls-1" d="..."></path></g></svg>`


## For This project: PROJECT NAME

Write here any specific information for the project you're working on


## Testing

* Test if font Icons are the best choice
* Be aware that FontAwesome and Material Design Icons are being used with the searchbox
